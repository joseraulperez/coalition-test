<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Product $product)
    {
        return response()->json(
            $product->getProducts()
        );
    }

    public function store(Request $request, Product $product)
    {
        $this->validate($request, [
            'name' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);

        return response()->json(
            $product->addProduct($request->name, $request->quantity, $request->price)
        );
    }
}
