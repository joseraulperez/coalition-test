<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class InventoryController extends Controller
{
    /**
     * Show the inventory.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('inventory');
    }
}
