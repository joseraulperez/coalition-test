<?php

namespace App;

use Illuminate\Support\Facades\Storage;

class Product
{
    const FILE_NAME = 'products.json';

    /**
     * Get all products.
     *
     * @return array|mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getProducts()
    {
        if (Storage::disk('local')->exists(self::FILE_NAME)) {
            return json_decode(Storage::disk('local')->get(self::FILE_NAME));
        }

        return [];
    }

    /**
     * Add a new element to the list.
     *
     * @param $name
     * @param $quantity
     * @param $price
     * @return array
     */
    public function addProduct($name, $quantity, $price)
    {
        $products = $this->getProducts();

        $product = [
          'name' => $name,
          'quantity' => $quantity,
          'price' => $price,
          'created_at' => date('Y-m-d H:i:s'),
        ];

        array_push($products, $product);

        Storage::disk('local')->put(self::FILE_NAME, json_encode($products));

        return $product;
    }
}