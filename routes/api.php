<?php

use Illuminate\Http\Request;

Route::get('/products', [
    'uses'      => 'Api\ProductsController@index',
    'as'        => 'api.products',
]);

Route::post('/products', [
    'uses'      => 'Api\ProductsController@store',
    'as'        => 'api.products.store',
]);